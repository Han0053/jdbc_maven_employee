package com.tgl.mysql.employee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;
import java.text.DecimalFormat;

public class CRUD {
	Scanner input = new Scanner(System.in);
	DecimalFormat df = new DecimalFormat("#####0.00");

	String url = "jdbc:mysql://localhost:3306/mysql?useTimezone=true&serverTimezone=GMT%2B8";
	// MySQL user
	String user = "root";
	// MySQL password
	String password = "gfHan0322";

	public void create(int height, int weight, String engName, String chiName, String phone, String email, double bmi) {

		try (Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement stmt = con.prepareStatement("INSERT INTO employee (height, weight, engName, "
			+ "chiName, phone, " + "email, bmi) " + "VALUES(?, ?, ?, ?, ?, ?, ?)")) {
			
			stmt.setInt(1, height);
			stmt.setInt(2, weight);
			stmt.setString(3, engName);
			stmt.setString(4, chiName);
			stmt.setString(5, phone);
			stmt.setString(6, email);
			stmt.setString(7, df.format(bmi));
			
			stmt.executeUpdate();
			System.out.println("Create success");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delete(int delId) {

		try (Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement stmt = con.prepareStatement("DELETE FROM employee WHERE id = ?")) {
			
			stmt.setInt(1, delId);
			stmt.executeUpdate();
			System.out.println("Delete success");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(int delId, String newEngName) {
		
		try (Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement stmt = con.prepareStatement("UPDATE employee SET engName = ? WHERE id = ? ORDER BY height DESC");){
			
			stmt.setString(1, newEngName);
			stmt.setInt(2, delId);
			
			stmt.executeUpdate();
			System.out.println("update success");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(String.format("emp: %s, exception", e));
		}
	}

	public void read(int bdHei, Double bdBmi) { 
		
		List<String> list = new ArrayList<String>();
		
		try (Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement()){
			
			String sel = "SELECT * FROM employee WHERE height > " + bdHei + " AND bmi > " + bdBmi + "ORDER BY height";
			
			ResultSet rs = stmt.executeQuery(sel);
			//int columnsNumber = rs.getColumnCount();
			
			while (rs.next()) {
				for (int i = 1; i <= 10; i++) {
					//list.
				}
			}
			
//			while (rs.next()) {
//				String Info = "";
//			    for (int i = 1; i <= columnsNumber; i++) {
//			        if(rs.getString(i).matches("[\\u4E00-\\u9FA5]+")) {
//			        	for(int j = 1; j <= rs.getString(i).length(); j += 2)
//			            {
//			        		char s[] = {rs.getString(i).charAt(j-1)};
//			                String temp = new String(s);
//
//			                if(j < rs.getString(i).length()) {
//			                	temp = temp + "X";
//			                }
//			                Info = Info + temp;
//			             }
//			        }else {
//			        	Info = Info + rs.getString(i) + "\t";
//			        }
//			        //String columnValue = rs.getString(i);
//			    }
//			    System.out.println(Info);
//			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
