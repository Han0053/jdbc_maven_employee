package com.tgl.mysql.employee;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {
	Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws SQLException {
		Main main = new Main();

		Employee emp = new Employee();
		// emp.InsertData();
		CRUD crud = new CRUD();
		// crud.create();
		// crud.delete();
		// crud.update();
		// main.doCreate();
		// main.doDelete();
		// main.doUpdate();
		main.doRead();
	}

	public void doCreate() {

		System.out.println("Please enter your height: ");
		int height = input.nextInt();
		double hei = new Double(height);

		System.out.println("Please enter your weight: ");
		int weight = input.nextInt();
		double wei = new Double(weight);

		// System.out.println("Please enter your English name: ");
		String engName = "hank";

		// System.out.println("Please enter your Chinese name: ");
		String chiName = "王小明";

		// System.out.println("Please enter your phone number: ");
		String phone = "1234";

		// System.out.println("Please enter your email: ");
		String email = "hank@tgl.com.tw";

		double bmi = wei / Math.pow((hei / 100), 2);

		CRUD crud = new CRUD();

		crud.create(height, weight, engName, chiName, phone, email, bmi);
	}

	public void doDelete() {
		CRUD crud = new CRUD();
		System.out.println("delete id: ");
		crud.delete(input.nextInt());
	}

	public void doUpdate() {
		CRUD crud = new CRUD();

		System.out.println("update id: ");
		int delId = input.nextInt();
		// System.out.println("enter an new English name: ");
		String newEngName = "Hello-World";

		crud.update(delId, newEngName);
	}

	public void doRead() {
		CRUD crud = new CRUD();
		System.out.println("bound of height is: ");
		int bdHei = input.nextInt();
		System.out.println("bound of bmi is: ");
		Double bdBmi = input.nextDouble();
		
		crud.read(bdHei, bdBmi);
	}
}
