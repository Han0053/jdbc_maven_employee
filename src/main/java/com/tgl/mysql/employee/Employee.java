package com.tgl.mysql.employee;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.sql.Timestamp;

public class Employee {
	// driver name
	// String driver = "com.mysql.jdbc.Driver";
	// URL from database
	String url = "jdbc:mysql://localhost:3306/mysql?useTimezone=true&serverTimezone=GMT%2B8";
	// MySQL user
	String user = "root";
	// MySQL password
	String password = "gfHan0322";
	
	
	public void InsertData() {
		
		try (BufferedReader read = new BufferedReader(
				new InputStreamReader(new FileInputStream("D:\\test\\memberInfo.txt"), "UTF-8"))) {

			String line;
			while ((line = read.readLine()) != null) {
				String[] tmp = line.split(" ");
				if (tmp.length != 8) {
					System.out.println(String.format("raw string : %s length error", line));
					continue;
				}
				
				try (Connection con = DriverManager.getConnection(url, user, password);
					PreparedStatement stmt = con.prepareStatement("INSERT INTO employee (height, weight, engName, "
					+ "chiName, phone, email, bmi, update_time) "+ "VALUES(?, ?, ?, ?, ?, ?, ?,?)");) {
					
					String heightString = tmp[1]; // height
					int height = Integer.parseInt(heightString);
					stmt.setInt(1, height);

					String weightString = tmp[2];
					int weight = Integer.parseInt(weightString);
					stmt.setInt(2, weight);

					String engName = tmp[3];
					stmt.setString(3, engName);
					String chiName = tmp[4];
					stmt.setString(4, chiName);
					String phone = tmp[5];
					stmt.setString(5, phone);
					String email = tmp[6];
					stmt.setString(6, email);

					String bmiString = tmp[7];
					float bmi = Float.parseFloat(bmiString);
					stmt.setFloat(7, bmi);

					Date date = new Date();
					Timestamp timestamp = new Timestamp(date.getTime());
					stmt.setTimestamp(8, timestamp);

					stmt.executeUpdate();
				} catch (Exception e) {
					System.out.println(String.format("%s has error ", e.getMessage()));
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
